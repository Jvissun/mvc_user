package ru.jvissun;

import java.util.ArrayList;

public class Controller {
    private User userModel;
    private View view;

    public Controller(User userModel, View view) {
        this.userModel = userModel;
        this.view = view;
    }

    public ArrayList<User> getUserList() {
        return userModel.getUserList();
    }

    public int getUserId() { return userModel.getId(); }

    public String getUserName() { return userModel.getName(); }

    public void setUserName(String name) { userModel.setName(name); }

    public int getUserAge() { return userModel.getAge(); }

    public void setUserAge(int age) { userModel.setAge(age); }

    public boolean getUserSex() { return userModel.getSex(); }

    public void setUserSex(boolean sex) { userModel.setSex(sex); }

    public String getUserWorkTitle() { return userModel.getWorkTitle(); }

    public void setUserWorkTitle(String title) { userModel.setWorkTitle(title); }

    public String getUserDescription() { return userModel.getDescription(); }

    public void setUserDescription(String description) { userModel.setDescription(description); }

    //print info about user
    public void updateInfoView () {
        if (userModel.getName() != null && !userModel.getName().isEmpty() && userModel.getName().trim().length() > 0) {
            view.printInfoAboutUser(userModel.getId(), userModel.getName(), userModel.getAge(), userModel.getSex(),
                    userModel.getWorkTitle(), userModel.getDescription());
        }
    }

    //print similar users
    public void findFitUser() {
        if (userModel.getName() != null && !userModel.getName().isEmpty() && userModel.getName().trim().length() > 0) {

            if (getUserList().size() == 1) {
                view.printFit(false, userModel.getName(), null);
            } else if (getUserList().size() > 1) {
                for (User user : getUserList()) {
                    if (user != null && !userModel.equals(user)) {
                        if (userModel.getAge() == user.getAge() || userModel.getSex() == user.getSex() ||
                                userModel.getWorkTitle().equals(user.getWorkTitle()) || userModel.getDescription().equals(user.getDescription())) {
                            view.printFit(true, userModel.getName(), user.getName());
                        } else view.printFit(false, userModel.getName(), user.getName());
                    }
                }
            }
            System.out.println("------------------------------------------" + " BD size = " + getUserList().size());
        }
    }

}
