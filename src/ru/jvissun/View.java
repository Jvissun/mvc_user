package ru.jvissun;

public class View {

    public void printInfoAboutUser (int id, String name, int age, boolean sex, String workTitle, String description) {
        System.out.println(String.format(">>> User is: ID = %d, Name = %s, Age = %d, Sex = %b, Title = %s, Info = %s", id, name, age, sex, workTitle, description));
    }

    public void printFit(boolean fit, String name1, String name2) {
        String fitUser = String.format("OK. %s fit %s.", name1, name2);
        String differentUser = String.format("FAIL. %s does not fit %s.", name1, name2);

        if (name1 == null || name1.isEmpty() || name1.trim().length() == 0) {
            System.out.println("ERROR! User not found.");
        } else if (name2 == null || name2.isEmpty() || name2.trim().length() == 0) {
            System.out.println(String.format("FAIL. %s does not fit.", name1));
        } else
            System.out.println(!fit ? differentUser : fitUser);
    }
}
