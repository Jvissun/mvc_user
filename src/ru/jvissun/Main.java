package ru.jvissun;

/**
 * MVC. Created 18.06.2015.
 */

public class Main {

    public static void main(String[] args) {

        addUser("Lila", 38, false, "Sales manager", "Love money");
        addUser("Ivan", 25, true, "Programmer", "Like cat");
        addUser("Sergey", 22, true, "Designer", "Like dog");
        addUser("Anna", 20, false, "Manager", "Love Sergey");
        addUser("Eva", 25, false, "Manager", "Love Ivan");
        addUser("", 38, false, "Manager", "****");              //no valid
        addUser("   ", 40, false, "Manager", "**");             //no valid
        addUser(null, 18, false, "Manager", "****");            //no valid
        addUser("Alex", 30, true, "President", "Have status");


    }

    //Add new users and print info
    public static void addUser (String name, int age, boolean sex, String workTitle, String description) {
        User model = new User(name, age, sex, workTitle, description);
        View view = new View();
        Controller controller = new Controller(model, view);
        controller.updateInfoView();
        controller.findFitUser();
    }

}
