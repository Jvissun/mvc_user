package ru.jvissun;

import java.util.ArrayList;

public class User {

    private static ArrayList<User> userData = new ArrayList<User>();

    private static int id;
    private String name;
    private int age;
    private boolean sex;         //true - Male, false - Female
    private String workTitle;
    private String description;

    public User(String name, int age, boolean sex, String workTitle, String description) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.workTitle = workTitle;
        this.description = description;

        if (name != null && !name.isEmpty() && name.trim().length() > 0) {
            userData.add(this);
            id++;
        }
    }

    public ArrayList<User> getUserList() { return userData; }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public String getWorkTitle() {
        return workTitle;
    }

    public void setWorkTitle(String workTitle) {
        this.workTitle = workTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
